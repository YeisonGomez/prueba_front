(function() {
    'use strict';

    angular.module('singApp', [
        'singApp.core',
        'singApp.dashboard',
        'singApp.another',
        'singApp.profile',
        'singApp.login',
        'singApp.error',
        'singApp.stadistics',
        'singApp.order'
    ]);
})();
