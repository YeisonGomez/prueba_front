(function() {
  'use strict';

  var module = angular.module('singApp.order', [
    'ui.router'
  ]);

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider'];

  function appConfig($stateProvider) {
    $stateProvider
      .state('app.order', {
        url: '/order',
        templateUrl: 'app/modules/order/order.html',
        controller: 'OrderController'
      })

  }
})();
