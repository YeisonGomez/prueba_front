(function() {
    'use strict';

    orderService.$inject = ['JSONOrder'];

    function orderService(JSONOrder) {

        this.getOrder = function(){
            return JSONOrder;
        }
    }

    angular.module('singApp.order')
        .service('OrderService', orderService);
})();
