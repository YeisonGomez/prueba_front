(function() {
    'use strict';

    orderFactory.$inject = [];

    function orderFactory() {
        return [{
            provider: "Provedor 1",
            products: [
                { name: "Papa", state: "Pendiente" },
                { name: "Tomate", state: "Listo" },
                { name: "Cebolla", state: "Cancelado" }
            ]
        }, {
            provider: "Provedor 2",
            products: [
                { name: "Frijol", state: "Pendiente" },
                { name: "Lentejas", state: "Listo" },
                { name: "Ensalada", state: "Cancelado" }
            ]
        }, {
            provider: "Provedor 3",
            products: [
                { name: "Manzana", state: "Pendiente" },
                { name: "Fresa", state: "Listo" },
                { name: "Banano", state: "Cancelado" }
            ]
        }, ];
    }

    angular.module('singApp.order')
        .factory('JSONOrder', orderFactory);
})();
