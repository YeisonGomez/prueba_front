(function() {
    'use strict';

    OrderController.$inject = ['$scope', 'OrderService'];

    function OrderController($scope, OrderService) {

    	$scope.data_order = OrderService.getOrder();
    }

    angular.module('singApp.order')
        .controller('OrderController', OrderController);

})();
