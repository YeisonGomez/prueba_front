(function() {
    'use strict';

    stadisticsService.$inject = ['JSON'];

    function stadisticsService(JSON) {

        this.getStadisticsConsumer = function(){
            return JSON;
        }
    }

    angular.module('singApp.stadistics')
        .service('stadisticsService', stadisticsService);
})();
