(function() {
  'use strict';

  var module = angular.module('singApp.stadistics', [
    'ui.router',
    'chart.js'
  ]);

  module.config(appConfig);

  appConfig.$inject = ['$stateProvider', 'ChartJsProvider'];

  function appConfig($stateProvider, ChartJsProvider) {
    $stateProvider
      .state('app.stadistics', {
        url: '/stadistics',
        templateUrl: 'app/modules/stadistics/stadistics.html',
        controller: 'StadisticsController'
      })

    ChartJsProvider.setOptions({ colors : [ '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'] });
  }
})();
