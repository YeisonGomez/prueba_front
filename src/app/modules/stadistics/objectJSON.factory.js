(function() {
    'use strict';

    stadisticsFactory.$inject = [];

    function stadisticsFactory() {
        return [{
            month: "Enero",
            data: [{
                provider: "Provedor 1",
                bought: [
                    { product: "Papa", money: 50000 },
                    { product: "Tomate", money: 60000 },
                    { product: "Arroz", money: 100000 }
                ]
            }, {
                provider: "Provedor 2",
                bought: [
                    { product: "Spaguettis", money: 30000 },
                    { product: "Tomate", money: 80000 },
                    { product: "Cebolla", money: 1000 },
                    { product: "Mucha lenteja", money: 200000 }
                ]
            }, {
                provider: "Provedor 3",
                bought: [
                    { product: "Frijol", money: 60000 },
                    { product: "Lentejas", money: 1000 }
                ]
            }]
        }, 
        {
            month: "Febrero",
            data: [{
                provider: "Provedor 1",
                bought: [
                    { product: "Papa", money: 500000 },
                    { product: "Tomate", money: 80000 },
                    { product: "Arroz", money: 30000 }
                ]
            }, {
                provider: "Provedor 2",
                bought: [
                    { product: "Spaguettis", money: 450000 },
                    { product: "Tomate", money: 84000 },
                    { product: "Cebolla", money: 10000 },
                    { product: "Mucha lenteja", money: 20000 }
                ]
            }, {
                provider: "Provedor 3",
                bought: [
                    { product: "Frijol", money: 690000 },
                    { product: "Lentejas", money: 8000 }
                ]
            }]
        },
        {
            month: "Marzo",
            data: [{
                provider: "Provedor 1",
                bought: [
                    { product: "Papa", money: 8000 },
                    { product: "Tomate", money: 320000 },
                    { product: "Arroz", money: 150000 }
                ]
            }, {
                provider: "Provedor 2",
                bought: [
                    { product: "Spaguettis", money: 34000 },
                    { product: "Tomate", money: 85000 },
                    { product: "Cebolla", money: 10000 },
                    { product: "Mucha lenteja", money: 250000 }
                ]
            }, {
                provider: "Provedor 3",
                bought: [
                    { product: "Frijol", money: 62000 },
                    { product: "Lentejas", money: 43000 }
                ]
            }]
        }
        ];
    }

    angular.module('singApp.stadistics')
        .factory('JSON', stadisticsFactory);
})();
