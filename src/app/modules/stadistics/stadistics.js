(function() {
    'use strict';

    stadisticsController.$inject = ['$scope', 'stadisticsService'];

    function stadisticsController($scope, stadisticsService) {
        $scope.data_restaurant = stadisticsService.getStadisticsConsumer();

        $scope.line_series = [];
        $scope.line_data = [];
        $scope.line_labels = ["Enero", "Febrero", "Marzo", "Abril"];

        $scope.doughnut_labels = [];
  		$scope.doughnut_data = [];

        addDataLineChart();
        addDataDoughnutChart();
        configCharts();

        $scope.onClick = function(points, evt) {
            console.log(points, evt);
        };

        function addDataDoughnutChart(){
            for (var i = 0; i < $scope.line_data.length; i++) {
                var money_total = 0;
                for (var j = 0; j < $scope.line_data[i].length; j++) {
                    money_total += $scope.line_data[i][j];
                }
                $scope.doughnut_data.push(money_total);
            }
        }

        function addDataLineChart() {
            var data_line = [];
            var provider = [];
            for (var i = 0; i < $scope.data_restaurant.length; i++) { //Mes
                var data_month = $scope.data_restaurant[i];
                for (var j = 0; j < data_month.data.length; j++) { //Provedor
                    if(data_line[j] == undefined){
                        data_line.push([]);    
                    }
                    var data_bought = data_month.data[j].bought;
                    var money_total = 0;
                    for (var l = 0; l < data_bought.length; l++) { //Comprado
                        money_total += data_bought[l].money;
                    }
                    data_line[j].push(money_total);
                }
                $scope.line_series.push(data_month.data[i].provider);
                $scope.doughnut_labels.push(data_month.data[i].provider);
            }
            $scope.line_data = data_line;
        }

        function configCharts() {
            $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
            $scope.line_options = {
                scales: {
                    yAxes: [{
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    }, {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }]
                }
            };
        }
    }

    angular.module('singApp.stadistics')
        .controller('StadisticsController', stadisticsController);

})();
